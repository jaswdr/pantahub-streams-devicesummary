/*
 * Copyright 2018  Pantacor Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package pantahub;

import static org.apache.kafka.streams.StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG;

import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Serialized;
import org.bson.BsonDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.JsonNode;

import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;

@SpringBootApplication(exclude = { MongoAutoConfiguration.class, MongoDataAutoConfiguration.class })
public class DeviceSummary2 {

	@Value("${pantahub.kafka.hostport:localhost:9092}")
	private String kafkaHostPort;

	@Value("${pantahub.schemaregistry.url:http://localhost:8081}")
	private String schemaRegistryUrl;

	@Value("${pantahub.kafka.streams.id:ph-devicesummary-dev}")
	private String kafkaStreamsId;

	@Value("${pantahub.kafka.streams.max-threads:10}")
	private int kafkaStreamsMaxThreads;

	@Value("${pantahub.kafka.topic.internal.replication.factor:1}")
	private int kafkaTopicInternalReplicationFactor;

	@Value("${pantahub.kafka.streams.commit-interval-ms:3000}")
	private long kafkaStreamsCommitIntervalMs;

	@Value("${pantahub.kafka.topic.prefix:pantabase1.pantabase-serv}")
	private String topicPrefix;

	@Value("${pantahub.kafka.topic.summary:ph-device-summary-short-dev}")
	private String summaryTopicName;

    @Value("${pantahub.kafka.topic.fluentd:fluentd-api}")
	private String fluentdTopicName;

	public void runApp() throws Exception {
		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaStreamsId);
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHostPort);
		props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, kafkaStreamsMaxThreads);
		props.put("schema.registry.url", schemaRegistryUrl);
		props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, kafkaStreamsCommitIntervalMs);

		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
		props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
		props.put(DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
				org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class);
		props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, kafkaTopicInternalReplicationFactor);

		final StreamsBuilder builder = new StreamsBuilder();

		KStream<String, GenericRecord> devices = builder.stream(topicPrefix + ".pantahub_devices");

		KStream<String, DeviceSummaryShort> devicesByPrn = devices.mapValues((v) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder().build();

			BsonDocument doc = null;

			if (v == null) {
				System.out.println("No document in device mapValues??");
				return null;
			}
			if (v.get("after") != null) {
				String afterString = (String) v.get("after");
				doc = BsonDocument.parse(afterString.toString());
			}
			if (v.get("patch") != null) {
				String patchString = (String) v.get("patch");
				doc = BsonDocument.parse(patchString.toString());
				if (doc.get("$set") != null) {
					doc = doc.getDocument("$set");
				}
			}

			if (doc == null) {
				System.out.println("No after in mongo document -- K:  || V: " + v.toString());
				return null;
			}

			if (doc.get("_id") != null && doc.getObjectId("_id") != null) {
				deviceSummaryShort.deviceid = doc.getObjectId("_id").getValue().toHexString();
			}
			if (doc.get("prn") != null && doc.getString("prn") != null) {
				deviceSummaryShort.device = doc.getString("prn").getValue();
			}
			if (doc.get("ispublic") != null && doc.get("ispublic").isBoolean()) {
				deviceSummaryShort.public$ = doc.getBoolean("ispublic").getValue();
			}
			if (doc.get("nick") != null && doc.getString("nick") != null) {
				deviceSummaryShort.device_nick = doc.getString("nick").getValue();
			}
			if (doc.get("owner") != null && doc.getString("owner") != null) {
				deviceSummaryShort.owner = doc.getString("owner").getValue();
			}
			if (doc.get("timemodified") != null && doc.getDateTime("timemodified") != null) {
				deviceSummaryShort.timestamp = doc.getDateTime("timemodified").asDateTime().getValue();
			}
			if (doc.get("garbage") != null && doc.get("garbage").isBoolean()) {
				deviceSummaryShort.garbage = doc.getBoolean("garbage").asBoolean().getValue();
			}

			if (doc.get("user-meta") != null && doc.get("user-meta").isDocument()
					&& doc.getDocument("user-meta") != null) {
				if (doc.getDocument("user-meta").get("api-fleet-pantahub-com/groups") != null
						&& doc.getDocument("user-meta").get("api-fleet-pantahub-com/groups").isString()) {
					deviceSummaryShort.setFleetGroup(
							doc.getDocument("user-meta").getString("api-fleet-pantahub-com/groups").getValue());
				}
				if (doc.getDocument("user-meta").get("api-fleet-pantahub-com/models") != null
						&& doc.getDocument("user-meta").get("api-fleet-pantahub-com/models").isString()) {
					deviceSummaryShort.setFleetModel(
							doc.getDocument("user-meta").getString("api-fleet-pantahub-com/models").getValue());
				}
				if (doc.getDocument("user-meta").get("api-fleet-pantahub-com/location") != null
						&& doc.getDocument("user-meta").get("api-fleet-pantahub-com/location").isString()) {
					deviceSummaryShort.setFleetLocation(
							doc.getDocument("user-meta").getString("api-fleet-pantahub-com/location").getValue());
				}
				if (doc.getDocument("user-meta").get("api-fleet-pantahub-com/rev") != null
						&& doc.getDocument("user-meta").get("api-fleet-pantahub-com/rev").isString()) {
					deviceSummaryShort.setFleetRev(
							doc.getDocument("user-meta").getString("api-fleet-pantahub-com/rev").getValue());
				}
				if (doc.getDocument("user-meta").get("api-fleet-pantahub-com/type") != null
						&& doc.getDocument("user-meta").get("api-fleet-pantahub-com/type").isString()) {
					deviceSummaryShort.setFleetType(
							doc.getDocument("user-meta").getString("api-fleet-pantahub-com/type").getValue());
				}
			}
			return deviceSummaryShort;
		});

		KTable<String, DeviceSummaryShort> deviceChangelog = devicesByPrn.groupByKey().reduce((v1, v2) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder(v1).build();

			if (v2.deviceid != null) {
				deviceSummaryShort.deviceid = v2.deviceid;
			}

			deviceSummaryShort.public$ = v2.public$;

			if (v2.device != null) {
				deviceSummaryShort.device = v2.device;
			}
			if (v2.device_nick != null) {
				deviceSummaryShort.device_nick = v2.device_nick;
			}
			if (v2.owner != null) {
				deviceSummaryShort.owner = v2.owner;
			}
			if (v2.timestamp != null && v2.timestamp != 0) {
				deviceSummaryShort.timestamp = v2.timestamp;
			}
			if (v2.garbage != null) {
				deviceSummaryShort.garbage = v2.garbage;
			}
			if (v2.fleet_group != null) {
				deviceSummaryShort.fleet_group = v2.fleet_group;
			}
			if (v2.fleet_model != null) {
				deviceSummaryShort.fleet_model = v2.fleet_model;
			}
			if (v2.fleet_rev != null) {
				deviceSummaryShort.fleet_rev = v2.fleet_rev;
			}
			if (v2.fleet_location != null) {
				deviceSummaryShort.fleet_location = v2.fleet_location;
			}
			if (v2.fleet_type != null) {
				deviceSummaryShort.fleet_type = v2.fleet_type;
			}

			return deviceSummaryShort;
		});

		KTable<String, DeviceSummaryShort> deviceTable = deviceChangelog.toStream().selectKey((k, v) -> {
			return v.device;
		}).groupByKey().reduce((v1, v2) -> {
			if (v2 != null) {
				return v2;
			}
			return v1;
		});

		KStream<String, GenericRecord> trails = builder.stream(topicPrefix + ".pantahub_trails");
		KStream<String, DeviceSummaryShort> trailsByDevicePrn = trails.mapValues((v) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder().build();

			BsonDocument doc = null;
			if (v.get("after") != null) {
				String afterString = (String) v.get("after");
				doc = BsonDocument.parse(afterString.toString());
			}
			if (v.get("patch") != null) {
				String patchString = (String) v.get("patch");
				doc = BsonDocument.parse(patchString.toString());
				if (doc.get("$set") != null) {
					doc = doc.getDocument("$set");
				}
			}
			if (doc == null) {
				System.out.println("No after in mongo document -- K:  || V: " + v.toString());
				return null;
			}
			if (doc.get("device") != null) {
				deviceSummaryShort.device = doc.getString("device").getValue();
			}
			if (doc.get("last-touched") != null) {
				deviceSummaryShort.timestamp = doc.getDateTime("last-touched").asDateTime().getValue();
			}

			return deviceSummaryShort;
		});

		KTable<String, DeviceSummaryShort> trailsByDeviceChangelog = trailsByDevicePrn.groupByKey().reduce((v1, v2) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder(v1).build();

			if (v2.device != null) {
				deviceSummaryShort.device = v2.device;
			}
			if (v2.timestamp != null && v2.timestamp != 0) {
				deviceSummaryShort.timestamp = v2.timestamp;
			}
			return deviceSummaryShort;
		});

		KTable<String, DeviceSummaryShort> trailsByDeviceTable = trailsByDeviceChangelog.toStream()
				.selectKey((k, v) -> {
					return v.device;
				}).groupByKey().reduce((v1, v2) -> {
					if (v2 != null) {
						return v2;
					}
					return v1;
				});

		KStream<String, GenericRecord> steps = builder.stream(topicPrefix + ".pantahub_steps");
		KStream<String, DeviceSummaryShort> stepsByDevicePrn = steps.mapValues((v) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder().build();

			BsonDocument doc = null;
			if (v.get("after") != null) {
				String afterString = (String) v.get("after");
				doc = BsonDocument.parse(afterString.toString());
			}
			if (v.get("patch") != null) {
				String patchString = (String) v.get("patch");
				doc = BsonDocument.parse(patchString.toString());
				if (doc.get("$set") != null) {
					doc = doc.getDocument("$set");
				}
			}
			if (doc == null) {
				System.out.println("No after in mongo document -- K:  || V: " + v.toString());
				return null;
			}
			if (doc.get("device") != null) {
				deviceSummaryShort.device = doc.getString("device").getValue();
			}

			if (doc.get("rev") != null) {
				if (doc.get("rev").isInt64()) {
					deviceSummaryShort.progress_revision = doc.getInt64("rev").intValue();
				} else if (doc.get("rev").isInt32()) {
					deviceSummaryShort.progress_revision = doc.getInt32("rev").intValue();
				} else {
					deviceSummaryShort.progress_revision = doc.getNumber("rev").intValue();
				}
			}

			if (doc.get("progress") != null) {
				if (doc.getDocument("progress").get("status") != null) {
					String progressStatus = doc.getDocument("progress").getString("status").getValue();
					deviceSummaryShort.status = progressStatus;
				}
				if (doc.getDocument("progress").get("statusmsg") != null) {
					deviceSummaryShort.status_msg = doc.getDocument("progress").getString("statusmsg").getValue();
				}
				if (doc.getDocument("progress").get("progress") != null) {
					if (doc.getDocument("progress").get("progress").isInt64()) {
						deviceSummaryShort.progress = doc.getDocument("progress").getInt64("progress").intValue();
					} else {
						deviceSummaryShort.progress = doc.getDocument("progress").getNumber("progress").intValue();
					}
				}
			}

			if (doc.get("progress-time") != null) {
				deviceSummaryShort.progress_time = doc.getDateTime("progress-time").asDateTime().getValue();
			}

			if (doc.get("step-time") != null) {
				deviceSummaryShort.step_time = doc.getDateTime("step-time").asDateTime().getValue();
			}

			if (doc.get("statesha") != null) {
				deviceSummaryShort.state_sha256 = doc.getString("statesha").getValue();
			}

			if (deviceSummaryShort.step_time == null) {
				deviceSummaryShort.trail_touched_time = deviceSummaryShort.progress_time;
			} else if (deviceSummaryShort.progress_time == null) {
				deviceSummaryShort.trail_touched_time = deviceSummaryShort.step_time;
			} else {
				deviceSummaryShort.trail_touched_time = deviceSummaryShort.progress_time > deviceSummaryShort.step_time
						? deviceSummaryShort.progress_time
						: deviceSummaryShort.step_time;
			}

			return deviceSummaryShort;

		});

		KTable<String, DeviceSummaryShort> stepsByDeviceChangelog = stepsByDevicePrn.groupByKey().reduce((v1, v2) -> {
			DeviceSummaryShort deviceSummaryShort = DeviceSummaryShort.newBuilder(v1).build();

			if (v2 == null) {
				return deviceSummaryShort;
			}
			if (v2.device != null) {
				deviceSummaryShort.device = v2.device;
			}
			if (v2.progress != null) {
				deviceSummaryShort.progress = v2.progress;
			}
			if (v2.progress_time != null && v2.progress_time != 0) {
				deviceSummaryShort.progress_time = v2.progress_time;
			}
			if (v2.step_time != null && v2.step_time != 0) {
				deviceSummaryShort.step_time = v2.step_time;
			}
			if (v2.trail_touched_time != null && v2.trail_touched_time != 0) {
				deviceSummaryShort.trail_touched_time = v2.trail_touched_time;
			}
			if (v2.status != null) {
				deviceSummaryShort.status = v2.status;
			}
			if (v2.status_msg != null) {
				deviceSummaryShort.status_msg = v2.status_msg;
			}
			if (v2.state_sha256 != null) {
				deviceSummaryShort.state_sha256 = v2.state_sha256;
			}
			if (!"NEW".equals(v1.status)) {
				if (v2 == null) {
					deviceSummaryShort.revision = v1.progress_revision;
				} else if (v1 == null) {
					deviceSummaryShort.revision = v2.progress_revision;
				} else if (v1.progress_revision == null) {
					deviceSummaryShort.revision = v2.progress_revision;
				} else if (v2.progress_revision == null) {
					deviceSummaryShort.revision = v1.progress_revision;
				} else if (v2.progress_revision > v1.progress_revision) {
					deviceSummaryShort.revision = v2.progress_revision;
				} else {
					deviceSummaryShort.revision = v1.progress_revision;
				}
			}

			return deviceSummaryShort;
		});

		KTable<String, DeviceSummaryShort> stepsByDeviceTable = stepsByDeviceChangelog.toStream().selectKey((k, v) -> {
			return v.device;
		}).groupByKey().reduce((v1, v2) -> {
			if (v2 != null) {
				return v2;
			}
			return v1;
		});

		// final HashMap<String, String> serdeConfig = new HashMap(props);
		// serdeConfig.put("schema.registry.url", schemaRegistryUrl);

		final Serde<String> keyStringSerde = Serdes.String();
		final Serde<JsonNode> valueJsonSerde = Serdes.serdeFrom(new JsonSerializer(), new JsonDeserializer());
		// final Serde<GenericRecord> valueGenericAvroSerde = new GenericAvroSerde();
		// valueGenericAvroSerde.configure(serdeConfig, false);

		KStream<String, JsonNode> accesslogs = builder.stream(fluentdTopicName,
				Consumed.with(keyStringSerde, valueJsonSerde));

		KTable<String, JsonNode> accesslogsbydevice = accesslogs.filter((key, value) -> {
			if (value.get("RemoteUser") == null)
				return false;

			String user = value.get("RemoteUser").asText();
			if (user.startsWith("prn:") && user.indexOf(":devices:") >= 0) {
				return true;
			}
			return false;
		}).selectKey((k, v) -> {
			return v.get("RemoteUser").asText();
		}).groupByKey(Serialized.with(keyStringSerde, valueJsonSerde)).reduce((v1, v2) -> {

			int t1 = (Integer) v1.get("Timestamp").asInt();
			int t2 = (Integer) v2.get("Timestamp").asInt();
			// to reduce processing load downstream, we only push new
			// event downstream if previous is older than 20sec.
			if (t1 + 20 < t2) {
				return v2;
			}
			return v1;
		}, Materialized.with(keyStringSerde, valueJsonSerde));

		KTable<String, DeviceSummaryShort> deviceSummaryShort = deviceTable.leftJoin(trailsByDeviceTable, (v1, v2) -> {
			DeviceSummaryShort result = DeviceSummaryShort.newBuilder(v1).build();

			if (v2 == null) {
				result.timestamp = v1.timestamp;
			} else if (v2.timestamp == null) {
				result.timestamp = v1.timestamp;
			} else if (v1.timestamp == null) {
				result.timestamp = v2.timestamp;
			} else if (v2.timestamp > v1.timestamp) {
				result.timestamp = v2.timestamp;
			}

			return result;
		}).leftJoin(stepsByDeviceTable, (v1, v2) -> {
			DeviceSummaryShort result = DeviceSummaryShort.newBuilder(v1).build();
			if (result.step_time == null) {
				result.step_time = 0L;
				result.trail_touched_time = 0L;
				result.progress_time = 0L;
			}
			if (v2 != null) {
				if (v2.progress != null)
					result.progress = v2.progress;

				if (v2.progress_revision != null)
					result.progress_revision = v2.progress_revision;

				if (v2.progress_time != null)
					result.progress_time = v2.progress_time;

				if (v2.revision != null)
					result.revision = v2.revision;

				if (v2.state_sha256 != null)
					result.state_sha256 = v2.state_sha256;

				if (v2.status != null)
					result.status = v2.status;

				if (v2.status_msg != null)
					result.status_msg = v2.status_msg;

				if (v2.step_time != null)
					result.step_time = v2.step_time;

				if (v2.trail_touched_time != null)
					result.trail_touched_time = v2.step_time;

			}

			return result;
		}).leftJoin(accesslogsbydevice, (v1, v2) -> {
			DeviceSummaryShort result = DeviceSummaryShort.newBuilder(v1).build();

			if (v2 != null) {
				Integer tsint = (Integer) v2.get("Timestamp").asInt();
				Long tslong = tsint * 1000L;
				Calendar cal1 = Calendar.getInstance();
				cal1.setTimeInMillis(tslong);

				Calendar cal2 = Calendar.getInstance();
				cal2.setTimeInMillis(result.timestamp);

				if (cal1.after(cal2))
					result.timestamp = tslong;

				if (v2.get("ReqHeaders") != null && v2.get("ReqHeaders").get("X-Real-Ip") != null
						&& v2.get("ReqHeaders").get("X-Real-Ip").get(0) != null) {
					result.real_ip = v2.get("ReqHeaders").get("X-Real-Ip").get(0).asText();
				}
			}

			return result;
		});

		deviceSummaryShort.toStream().map((k, v) -> {
			return new KeyValue<String, DeviceSummaryShort>(null, v);
		}).to(summaryTopicName);

		KStream<String, DeviceSummaryShortO> outS = deviceSummaryShort.toStream().map((k, v) -> {
			DeviceSummaryShortO out = new DeviceSummaryShortO();
			out.timestamp = v.timestamp == null ? 0 : v.timestamp;
			out.deviceid = v.deviceid == null ? "" : v.deviceid;
			out.device = v.device == null ? "" : v.device;
			out.device_nick = v.device_nick == null ? "" : v.device_nick;
			out.owner = v.owner == null ? "" : v.owner;
			out.revision = v.revision == null ? 0 : v.revision;
			out.progress_revision = v.progress_revision == null ? 0 : v.progress_revision;
			out.progress = v.progress == null ? 0 : v.progress;
			out.public$ = v.public$;
			out.status_msg = v.status_msg == null ? "" : v.status_msg;
			out.status = v.status == null ? "" : v.status;
			out.state_sha256 = v.state_sha256 == null ? "" : v.state_sha256;
			out.step_time = v.step_time == null ? 0L : v.step_time;
			out.progress_time = v.progress_time == null ? 0L : v.progress_time;
			out.trail_touched_time = v.trail_touched_time == null ? 0L : v.trail_touched_time;
			out.real_ip = v.real_ip == null ? "" : v.real_ip;
			out.fleet_group = v.fleet_group == null ? "" : v.fleet_group;
			out.fleet_rev = v.fleet_rev == null ? "" : v.fleet_rev;
			out.fleet_location = v.fleet_location == null ? "" : v.fleet_location;
			out.fleet_model = v.fleet_model == null ? "" : v.fleet_model;
			out.fleet_type = v.fleet_type == null ? "" : v.fleet_type;
			out.garbage = v.garbage == null ? false : v.garbage;
			return new KeyValue<String, DeviceSummaryShortO>(null, out);
		});

		outS.to(summaryTopicName + "-flat");
		outS.print(Printed.toSysOut());

		final Topology topology = builder.build();
		final KafkaStreams streams = new KafkaStreams(topology, props);
		final CountDownLatch latch = new CountDownLatch(1);

		Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
			@Override
			public void run() {
				streams.close();
				latch.countDown();
			}
		});

		try {
			streams.start();
			latch.await();
		} catch (

		Throwable e) {
			System.exit(1);
		}
		System.exit(0);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			this.runApp();
		};
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(DeviceSummary2.class, args);
	}
}
